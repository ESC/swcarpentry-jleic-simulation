---
title: "Analyzing the Standard Output"
teaching: 10
exercises: 0
questions:
- "What information if available in the standard output?"
objectives:
- "Understand the data structure of the output file."
- "Know how to access the variable defintions."
keypoints:
- "Standard output contains generated and reconstructed hit and track information."
---
FIXME

{% include links.md %}

