---
title: "Using Docker Containers"
teaching: 5
exercises: 5
questions:
- "How can I most quickly download and use the latest JLEIC simulation software?"
objectives:
- "Understand container instantiation, versioning and cleanup."
keypoints:
- "Docker containers allow you to use the JLEIC simulation software without needing to compile any code."
---
FIXME

{% include links.md %}

