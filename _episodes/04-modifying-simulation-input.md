---
title: "Modifying the Simulation Input"
teaching: 0
exercises: 0
questions:
- "How can I use my Monte Carlo generator as input for the simulation?"
objectives:
- "Understand how the simulation processes and reweights generated events."
- "Understand the supported Monte Carlo formats."
- "Know which Monte Carlo event generators can produce those files."
keypoints:
- "By using standard Monte Carlo event generators as input, you can easily obtain results for your physics interest with the simulation."
---
FIXME

{% include links.md %}

