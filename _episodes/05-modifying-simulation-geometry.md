---
title: "Modifying the Simulation Geometry"
teaching: 0
exercises: 0
questions:
- "How do I make slight adjustments to the existing implemented geometry?"
objectives:
- "Understand the structure of geometries in Geant4."
- "Understand how the geometry is organized in the simulation."
- "Understand how to replace or add detectors to the simulation"
- "Know how to change detector geometry information."
keypoints:
- "Geometry in this simulation is organized hierachically and individual elements can be modified, edited, or added. "
---
FIXME

{% include links.md %}

