---
title: "Running a Standard Example"
teaching: 5
exercises: 5
questions:
- "How do I run a simple standard simulation using the latest JLEIC physics and geometry?"
objectives:
- "Know how to start the simulation interactively."
- "Be able to load the standard geometry."
- "Start a simulation with a small number of events."
keypoints:
- "A standard simulation macro is provided with the JLEIC simulation software to easily run a few events."
---
FIXME

{% include links.md %}

