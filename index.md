---
layout: lesson
root: .  # Is the only page that doesn't follow the pattern /:path/index.html
permalink: index.html  # Is the only page that doesn't follow the pattern /:path/index.html
---
This tutorial lesson on the JLEIC simulation software is aimed at...

At the end of the tutorial you will be able to...

> ## Prerequisites
>
> We assume that you are generally familiar with the software environment of nuclear, hadronic,
> or particle physics experiments, from event generation and simulation, through hit selection
> and track finding, to histogramming and fitting of physical quantities.
> 
> We assume that you know how to navigate on a command-line based Linux system, either locally
> or on a central server.
{: .prereq}

{% include links.md %}
